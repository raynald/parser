import static com.google.common.base.Preconditions.checkArgument;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import javax.annotation.Nonnull;
import jdk.nashorn.internal.ir.annotations.Immutable;
import org.apache.commons.lang3.StringUtils;
import org.immutables.value.Value;

/**
 * Value class used to deserialize candidate info.
 */
@Immutable
@Value.Immutable
@JsonSerialize(as = ImmutableCandidate.class)
@JsonDeserialize(as = ImmutableCandidate.class)
public abstract class Candidate {

  enum field {
    id,
    candidateName,
    extraTime
  }

  /**
   * Convert a candidate object to a hashmap
   *
   * @param candidate Candidate object
   * @return hashmap representation of a candidate object
   */
  public static Map<String, String> toStringMapWithoutID(Candidate candidate) {
    return ImmutableMap.of(field.candidateName.toString(), candidate.getCandidateName(),
        field.extraTime.toString(), String.valueOf(candidate.getExtraTime()));
  }

  @Value.Check
  @SuppressWarnings("unused")
  void validate() {
    checkArgument(getId() > 0, "id must be greater than 0");
    checkArgument(StringUtils.isNotBlank(getCandidateName()), "candidate name must not be empty");
    checkArgument(getExtraTime() >= 0, "extra time must be great or equal to 0");
  }

  public abstract int getId();

  @Nonnull
  public abstract String getCandidateName();

  public abstract int getExtraTime();
}
