import static com.google.common.base.Preconditions.checkArgument;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableMap;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import javax.annotation.Nonnull;
import jdk.nashorn.internal.ir.annotations.Immutable;
import org.apache.commons.lang3.StringUtils;
import org.immutables.value.Value;

/**
 * Value class used to deserialize meta info.
 */
@Immutable
@Value.Immutable
@JsonSerialize(as = ImmutableMeta.class)
@JsonDeserialize(as = ImmutableMeta.class)
public abstract class Meta {

  enum field {
    title,
    startTime,
    endTime
  }

  /**
   * Convert a Meta object to hashmap
   *
   * @param meta Meta object
   * @return hashmap representation of a meta object
   */
  public static Map<String, String> toStringMap(Meta meta) {
    return ImmutableMap
        .of(field.title.toString(), meta.getTitle(), field.startTime.toString(),
            toTimeWithZoneInfo(meta.getStartTime()), field.endTime.toString(),
            toTimeWithZoneInfo(meta.getEndTime()));
  }

  private static String toTimeWithZoneInfo(Date origin) {
    DateFormat out = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
    out.setTimeZone(TimeZone.getTimeZone("Europe/Oslo")); // We would like to get the time in Oslo
    return out.format(origin);
  }

  @Value.Check
  @SuppressWarnings("unused")
  void validate() {
    checkArgument(StringUtils.isNotBlank(getTitle()),
        "Title can't be a blank string");
  }

  @Nonnull
  public abstract String getTitle();

  @Nonnull
  public abstract Date getStartTime();

  @Nonnull
  public abstract Date getEndTime();

}
