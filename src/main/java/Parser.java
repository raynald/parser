import static java.util.Objects.requireNonNull;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This class consists of utility functions for json
 */
public class Parser {

  private static final Logger logger = LogManager.getLogger(Parser.class);

  final private ObjectMapper mapper = new ObjectMapper();

  /**
   * Given two JsonNode objects, returns the JsonNode object with compared results
   *
   * @param before JsonNode object of original values
   * @param after JsonNode object of reference values
   * @return JsonNode object of compared results
   * @throws NullPointerException if any of the given argument is null
   */
  @Nonnull
  public JsonNode parse(@Nonnull JsonNode before, @Nonnull JsonNode after) throws Exception {
    requireNonNull(before);
    requireNonNull(after);

    final String META = "meta";
    final String CANDIDATES = "candidates";
    final String ID = "id";
    BasicConfigurator.configure();

    // Basic id check
    if (before.get(ID) != after.get(ID)) {
      throw new IllegalArgumentException("id of the two JsonNode objects are not same");
    }
    try {
      Map<String, String> metaMapBefore = getMetaMap(before.get(META));
      Map<Integer, Map<String, String>> candidatesListBefore = getCandidatesMap(
          before.get(CANDIDATES));

      Map<String, String> metaMapAfter = getMetaMap(after.get(META));
      Map<Integer, Map<String, String>> candidatesListAfter = getCandidatesMap(
          after.get(CANDIDATES));

      List<Map<String, String>> metaDiffResult = computeMetaDiff(metaMapBefore, metaMapAfter);

      Map<String, List<Map<String, Integer>>> candidatesDiffResult = computeCandidatesDiff(
          candidatesListBefore, candidatesListAfter);

      return mapper
          .valueToTree(ImmutableMap.of(META, metaDiffResult, CANDIDATES, candidatesDiffResult));
    } catch (InvalidDefinitionException ex) {
      throw ex;
    } catch (IOException ex) {
      logger.error("Error while parse JsonNode, message: " + ex);
      return null;
    }
  }

  /**
   * Compute the result meta field from two meta hashmaps
   *
   * @param mapBefore computed hashmap of meta from before.json
   * @param mapAfter computed hashmap of meta from after.json
   * @return computed list of hashmap according the assignment
   * @throws NullPointerException if any of the given argument is null
   */
  @Nonnull
  private List<Map<String, String>> computeMetaDiff(@Nonnull Map<String, String> mapBefore,
      @Nonnull Map<String, String> mapAfter) {
    requireNonNull(mapBefore);
    requireNonNull(mapAfter);

    final String FIELD = "field";
    final String BEFORE = "before";
    final String AFTER = "after";

    final List<Map<String, String>> diffResult = new ArrayList<Map<String, String>>();
    mapBefore.keySet().forEach(key -> {
      if (!mapBefore.get(key).equals(mapAfter.getOrDefault(key, ""))) {
        diffResult.add(ImmutableMap
            .of(FIELD, key, BEFORE, mapBefore.get(key), AFTER, mapAfter.getOrDefault(key, "")));
      }
    });
    mapAfter.keySet().forEach(key -> {
      if (!mapBefore.containsKey(key)) {
        diffResult.add(ImmutableMap.of(FIELD, key, BEFORE, "", AFTER, mapAfter.get(key)));
      }
    });
    return diffResult;
  }

  /**
   * Compute the result candidates field from two candidates hashmaps
   *
   * @param mapBefore computed hashmap of candidates from before.json
   * @param mapAfter computed hashmap of candidates from after.json
   * @return computed hashmap of list of hashmaps according to the assignment
   * @throws NullPointerException if any of the given argument is null
   */
  @Nonnull
  private Map<String, List<Map<String, Integer>>> computeCandidatesDiff(@Nonnull
      Map<Integer, Map<String, String>> mapBefore,
      @Nonnull Map<Integer, Map<String, String>> mapAfter) {
    requireNonNull(mapBefore);
    requireNonNull(mapAfter);

    final String ID = "id";
    final String EDITED = "edited";
    final String ADDED = "added";
    final String REMOVED = "removed";

    List<Map<String, Integer>> editedResult = new ArrayList<>();
    List<Map<String, Integer>> addedResult = new ArrayList<>();
    List<Map<String, Integer>> removedResult = new ArrayList<>();
    mapBefore.keySet().forEach(id -> {
      if (mapAfter.containsKey(id)) {
        if (!mapBefore.get(id).equals(mapAfter.get(id))) {
          editedResult.add(ImmutableMap.of(ID, id));
        }
      } else {
        removedResult.add(ImmutableMap.of(ID, id));
      }
    });
    mapAfter.keySet().stream().filter(id -> !mapBefore.containsKey(id))
        .forEach(id -> addedResult.add(ImmutableMap.of(ID, id)));
    return ImmutableMap.of(EDITED, editedResult, ADDED, addedResult, REMOVED, removedResult);
  }

  /**
   * Get required hashmap representation of meta from a JsonNode
   *
   * @param metaNode a JsonNode object of meta
   * @return hashmap representation of meta
   * @throws NullPointerException when {@code metaNode} is null
   */
  @Nonnull
  private Map<String, String> getMetaMap(@Nonnull JsonNode metaNode) {
    requireNonNull(metaNode);
    Meta meta = mapper.convertValue(metaNode, ImmutableMeta.class);
    return Meta.toStringMap(meta);
  }

  /**
   * Get required hashmap representation of candidates from a JsonNode
   *
   * @param candidatesNode a JsonNode object of candidates
   * @return hashmap representation of candidates
   * @throws IOException when some malformed or missing value in JsonNode
   * @throws NullPointerException when {@code candidatesNode} is null
   */
  @Nonnull
  private Map<Integer, Map<String, String>> getCandidatesMap(@Nonnull JsonNode candidatesNode)
      throws IOException {
    requireNonNull(candidatesNode);
    ObjectReader reader = mapper.readerFor(new TypeReference<List<ImmutableCandidate>>() {
    });
    List<ImmutableCandidate> result = reader.readValue(candidatesNode);
    return result.stream()
        .collect(Collectors.toMap(p -> p.getId(), p -> Candidate.toStringMapWithoutID(p)));
  }

}
