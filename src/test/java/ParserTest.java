import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * This class contains unittests for Parser
 */
public class ParserTest {

  private static final Logger logger = LogManager.getLogger(ParserTest.class);

  private Parser parser = new Parser();
  private ObjectMapper objectMapper = new ObjectMapper();
  private JsonNode beforeNode, afterNode, diffNode;

  private JsonNode getJsonNodeFromFile(String fileName) {
    try {
      String content = IOUtils.toString(
          this.getClass().getResourceAsStream(fileName),
          "UTF-8"
      );
      return objectMapper.readValue(content, JsonNode.class);
    } catch (IOException ex) {
      logger.error("Error while parsing Json String: " + ex);
    }
    return null;
  }

  @Before
  public void setup() {
    beforeNode = getJsonNodeFromFile("before.json");
    afterNode = getJsonNodeFromFile("after.json");
    diffNode = getJsonNodeFromFile("diff.json");
    BasicConfigurator.configure();
  }

  @Test
  public void parse_shouldReturnTheSameDiffNode() throws Exception {
    JsonNode resultNode = parser.parse(beforeNode, afterNode);
    assertEquals(resultNode, diffNode);
  }

  @Test
  public void parse_shouldThrowNPEWhenGivenNullBeforeNode() {
    assertThatThrownBy(() -> parser.parse(null, afterNode))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void parse_shouldThrowNPEWhenGivenNullAfterNode() {
    assertThatThrownBy(() -> parser.parse(beforeNode, null))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  public void parse_shouldThrowIAEWhenGivenMetaWithoutTitle() {
    JsonNode malformed = getJsonNodeFromFile("malformed_no_title.json");
    assertThatThrownBy(() -> parser.parse(malformed, afterNode))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void parse_shouldThrowIDEWhenGivenCandidateWithoutName() {
    JsonNode malformed = getJsonNodeFromFile("malformed_no_name.json");
    assertThatThrownBy(() -> parser.parse(malformed, afterNode))
        .isInstanceOf(InvalidDefinitionException.class);
  }

  @Test
  public void parse_shouldThrowIAEWhenGivenJsonWithDifferentID() {
    JsonNode differntId = getJsonNodeFromFile("different_id.json");
    assertThatThrownBy(() -> parser.parse(differntId, afterNode))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
