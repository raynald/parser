## How to run
- Install Maven
- Run "mvn install" to download dependencies

## Assumptions
- the original two json events should have same id to compare
- meta only has three given field, title should not be empty and date fields are valid 
- candidates is a list of candidate, which only has three fields. id should be positive, 
  name should not be empty and extra time should not be negative
- the result will return the datetime field with timezone (Oslo)

## Error
The original diff.json contains an error that returns the datetime field in UTC + 2. In fact,
January in Oslo is in timezone UTC + 1, so I have changed that.

## Implementation
- JsonObject has been replaced with JsonNode from Jackson
- Value class Candidate and Meta is used to deserialize and validate the data from json

## Author
Raynald Chung
